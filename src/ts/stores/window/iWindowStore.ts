export interface IWindowStore {
    title: string;

    updateTitle(title: string): void;
}
