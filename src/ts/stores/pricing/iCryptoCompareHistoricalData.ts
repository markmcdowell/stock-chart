interface ICryptoCompareOHLCPrice {
    close: number;
    high: number;
    low: number;
    open: number;
    time: number;
    volumefrom: number;
    volumeto: number;
}

interface ICryptoCompareHistoricalData {
    Aggregated: boolean;
    Data: ICryptoCompareOHLCPrice[];
    FirstValueInArray: boolean;
    HasWarning: boolean;
    Response: string;
    TimeFrom: number;
    TimeTo: number;
    Type: number;
}
