export interface IOHLCPrice {
    close: number;
    date: Date;
    high: number;
    low: number;
    open: number;
    volume: number;
}
