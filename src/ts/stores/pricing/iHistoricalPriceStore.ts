import { IOHLCPrice } from "./iOHLCPrice";

export interface IHistoricalPriceOptions {
    numberOfDays: number;
}

export interface IHistoricalPriceStore {
    loadData(symbol: string, options: IHistoricalPriceOptions): Promise<IOHLCPrice[]>;
}
